# Travel Time Aplication

Proyecto final de ingeniería de Software II

## Iniciar el proyecto

1. Instalar los requerimientos con pip (se recomienda utilizar virtualenv) con ```pip install -r requirements.txt```
2. Ejecutar ```pyro4-ns```
3. Ejecutar la aplicación web (carpeta web_page) con ```python app.py```
4. Ejecutar el servidor de aerolínea (carpeta server_airline) con ```python server.py```
5. Ejecutar el servidor de hoteles (carpeta server_hotel) con ```python server.py```
6. Ejecutar el servidor de agencia (carpeta server_agency) con ```python server.py```
7. Ejecutar el cliente (raíz del proyecto) ```python client.py```


## Componentes de la aplicación
* Servidor Agencia de Viajes
* Servidor Aerolínea
* Servidor Hotel
* Cliente Agencia de Viajes
* Aplicación Web
