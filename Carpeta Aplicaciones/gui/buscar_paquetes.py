import Pyro4
import json
import sys
from informacion_venta import Informacion
from PyQt5 import uic, QtWidgets
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QIcon, QColor
from PyQt5.QtWidgets import (QApplication, QDialog, QPushButton, QTableWidget,
                             QTableWidgetItem, QAbstractItemView, QHeaderView, QMenu,
                             QActionGroup, QAction, QMessageBox)

# Nombre del archivo aquí
qtCreatorFile = "buscar_paquetes.ui"
name_server = Pyro4.locateNS()
server_agency_uri = name_server.lookup('server_agency')
server_agency_remote = Pyro4.Proxy(server_agency_uri)


Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class BuscarPaquetes(QtWidgets.QMainWindow, Ui_MainWindow):
    """Clase BuscarPaquetes"""

    def __init__(self):
        """Inicializa la clase BuscarPaquetes"""
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.iniciarTablas()
        self.cargarClientes()
        # self.comboBoxGenero.addItem('M')
        # self.comboBoxGenero.addItem('F')
        # self.ButtonRegistrar.clicked.connect(self.registrar_usuario)

    def iniciarTablas(self):
        self.tablaClientes.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Deshabilitar el comportamiento de arrastrar y soltar
        self.tablaClientes.setDragDropOverwriteMode(False)

        # Seleccionar toda la fila
        self.tablaClientes.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Seleccionar una fila a la vez
        self.tablaClientes.setSelectionMode(QAbstractItemView.SingleSelection)

        # Especifica dónde deben aparecer los puntos suspensivos "..." cuando se muestran
        # textos que no encajan
        self.tablaClientes.setTextElideMode(Qt.ElideRight)# Qt.ElideNone

        # Establecer el ajuste de palabras del texto
        self.tablaClientes.setWordWrap(False)

        # Deshabilitar clasificación
        self.tablaClientes.setSortingEnabled(False)

        # Establecer el número de columnas
        self.tablaClientes.setColumnCount(2)

        # Establecer el número de filas
        self.tablaClientes.setRowCount(0)

        # Alineación del texto del encabezado
        self.tablaClientes.horizontalHeader().setDefaultAlignment(
            Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter
        )

        # Deshabilitar resaltado del texto del encabezado al seleccionar una fila
        self.tablaClientes.horizontalHeader().setHighlightSections(False)

        # Hacer que la última sección visible del encabezado ocupa todo el espacio disponible
        self.tablaClientes.horizontalHeader().setStretchLastSection(True)

        # Ocultar encabezado vertical
        self.tablaClientes.verticalHeader().setVisible(False)

        # Dibujar el fondo usando colores alternados
        self.tablaClientes.setAlternatingRowColors(True)

        # Establecer altura de las filas
        self.tablaClientes.verticalHeader().setDefaultSectionSize(20)

        # self.tablaClientes.verticalHeader().setHighlightSections(True)

        nombreColumnas = ("Cedula","Nombre")

        # Establecer las etiquetas de encabezado horizontal usando etiquetas
        self.tablaClientes.setHorizontalHeaderLabels(nombreColumnas)

        # Menú contextual
        # self.tablaClientes.setContextMenuPolicy(Qt.CustomContextMenu)
        # self.tablaClientes.customContextMenuRequested.connect(self.menuContextual)

        # Establecer ancho de las columnas
        for indice, ancho in enumerate((80, 120), start=0):
            self.tablaClientes.setColumnWidth(indice, ancho)

        self.tablaClientes.resize(700, 240)
        self.tablaClientes.move(20, 56)
        self.tablaClientes.cellClicked.connect(self.listarPaquetes)

        """Tabla Paquetes"""
        self.tablaPaquetes.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Deshabilitar el comportamiento de arrastrar y soltar
        self.tablaPaquetes.setDragDropOverwriteMode(False)

        # Seleccionar toda la fila
        self.tablaPaquetes.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Seleccionar una fila a la vez
        self.tablaPaquetes.setSelectionMode(QAbstractItemView.SingleSelection)

        # Especifica dónde deben aparecer los puntos suspensivos "..." cuando se muestran
        # textos que no encajan
        self.tablaPaquetes.setTextElideMode(Qt.ElideRight)# Qt.ElideNone

        # Establecer el ajuste de palabras del texto
        self.tablaPaquetes.setWordWrap(False)

        # Deshabilitar clasificación
        self.tablaPaquetes.setSortingEnabled(False)

        # Establecer el número de columnas
        self.tablaPaquetes.setColumnCount(2)

        # Establecer el número de filas
        self.tablaPaquetes.setRowCount(0)

        # Alineación del texto del encabezado
        self.tablaPaquetes.horizontalHeader().setDefaultAlignment(
            Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter
        )

        # Deshabilitar resaltado del texto del encabezado al seleccionar una fila
        self.tablaPaquetes.horizontalHeader().setHighlightSections(False)

        # Hacer que la última sección visible del encabezado ocupa todo el espacio disponible
        self.tablaPaquetes.horizontalHeader().setStretchLastSection(True)

        # Ocultar encabezado vertical
        self.tablaPaquetes.verticalHeader().setVisible(False)

        # Dibujar el fondo usando colores alternados
        self.tablaPaquetes.setAlternatingRowColors(True)

        # Establecer altura de las filas
        self.tablaPaquetes.verticalHeader().setDefaultSectionSize(20)

        # self.tablaPaquetes.verticalHeader().setHighlightSections(True)

        nombreColumnas = ("id","Fecha")

        # Establecer las etiquetas de encabezado horizontal usando etiquetas
        self.tablaPaquetes.setHorizontalHeaderLabels(nombreColumnas)

        # Menú contextual
        # self.tablaPaquetes.setContextMenuPolicy(Qt.CustomContextMenu)
        # self.tablaPaquetes.customContextMenuRequested.connect(self.menuContextual)

        # Establecer ancho de las columnas
        for indice, ancho in enumerate((80, 120), start=0):
            self.tablaPaquetes.setColumnWidth(indice, ancho)

        self.tablaPaquetes.resize(700, 240)
        self.tablaPaquetes.move(20, 56)
        self.tablaPaquetes.clearContents()

        self.tablaPaquetes.cellClicked.connect(self.resumen)

    def cargarClientes(self):
        self.tablaClientes.clearContents()
        data=server_agency_remote.get_clients()
        row = 0
        for cliente in data:
            self.tablaClientes.setRowCount(row + 1)
            self.tablaClientes.setItem(row, 0, QTableWidgetItem(str(cliente.get('id'))))
            self.tablaClientes.setItem(row, 1, QTableWidgetItem((cliente.get('names'))))
            row += 1

    def listarPaquetes(self,row):
        id=self.tablaClientes.item(row,0).text()
        data=server_agency_remote.get_packages(id)
        self.tablaPaquetes.clearContents()
        row = 0
        for pq in data:
            self.tablaPaquetes.setRowCount(row + 1)
            self.tablaPaquetes.setItem(row, 0, QTableWidgetItem(str(pq.get('id'))))
            self.tablaPaquetes.setItem(row, 1, QTableWidgetItem((pq.get('date'))))
            row += 1

    def resumen(self,row):
        id=self.tablaPaquetes.item(row,0).text()
        data=server_agency_remote.get_package_by_id(id)
        print(data)
        self.Informacion=Informacion(data)
        self.Informacion.show()


if __name__ == "__main__":
    """Funcion principal"""
    app =  QtWidgets.QApplication(sys.argv)
    window = BuscarPaquetes()
    window.show()
    sys.exit(app.exec_())
