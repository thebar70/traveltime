import sys
from PyQt5 import uic, QtWidgets

# Nombre del archivo aquí
qtCreatorFile = "resumen.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class Resumen(QtWidgets.QMainWindow, Ui_MainWindow):
    """Clase Resumen"""

    def __init__(self,resumen):
        """Inicializa la clase Resumen"""
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.res=resumen
        self.setupUi(self)
        self.llenarDatos()
        self.ButtonAceptar.clicked.connect(self.cerrar)

    def llenarDatos(self):
        """Suma Costos Agencia"""
        regiones=self.res['regiones']
        region=''
        for reg in regiones:
            region+=(reg[0])+', '

        paises=self.res['paises']
        pais=''
        for reg in paises:
            pais+=(reg[0])+', '

        ciudades=self.res['ciudades']
        ciudad=''
        for reg in ciudades:
            ciudad+=(reg[0])+', '

        actividades=self.res['actividades']
        actividad=''
        for reg in actividades:
            actividad+=(reg[0])+', '

        """Suma Costos Hoteles"""
        hoteles=self.res['hoteles']
        hotel=''
        for reg in hoteles:
            hotel+=(reg[0])+', '


        """suma Costos Aerolineas"""
        aerolineas=self.res['aerolineas']
        aerolinea=''
        for reg in aerolineas:
            aerolinea+=(reg[0])+', '

        numero_personas=self.res['numero_personas']
        self.lineEditRegiones.setText(region)
        self.lineEditPaises.setText(pais)
        self.lineEditCiudades.setText(ciudad)
        self.lineEditActividades.setText(actividad)
        self.lineEditHoteles.setText(hotel)
        self.lineEditAerolineas.setText(aerolinea)
        self.lineEditPersonas.setText(str(numero_personas))

    def cerrar(self):
        """Cierra la ventana"""
        print("cerrar")


if __name__ == "__main__":
    """Funcion principal"""
    app =  QtWidgets.QApplication(sys.argv)
    window = Resumen()
    window.show()
    sys.exit(app.exec_())
