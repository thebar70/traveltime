import sys
import Pyro4
from pago_paquete import PagoPaquete
from collections import namedtuple
from PyQt5 import uic, QtWidgets

# Nombre del fichero que contiene la GUI
qtCreatorFile = "venta_paquetes.ui"

# Variables globales para la comunicacion con el server
name_server = Pyro4.locateNS()
server_agency_uri = name_server.lookup('server_agency')
server_hotel_uri = name_server.lookup('server_hotel')
server_agency_remote = Pyro4.Proxy(server_agency_uri)
server_hotel_remote = Pyro4.Proxy(server_hotel_uri)

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

# Dependencias para el manejo de la GUI
from PyQt5.QtGui import QFont, QIcon, QColor
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QApplication, QDialog, QPushButton, QTableWidget,
                             QTableWidgetItem, QAbstractItemView, QHeaderView, QMenu,
                             QActionGroup, QAction, QMessageBox)

# Clase principal manejadora de la ventana
lista_regiones=[]
lista_paises=[]
lista_ciudades=[]
lista_actividades=[]
lista_hoteles=[]
lista_aerolineas=[]
resumen={'regiones':[],'paises':[],'ciudades':[],'actividades':[],'hoteles':[],'aerolineas':[],'numero_personas':1}

# Variable global que indica con que datos esta llena la trabla ciudades
seleccion_tabla=1


class VentaPaquetes(QtWidgets.QMainWindow, Ui_MainWindow):
    """Clase VentaPaquetes"""

    paquete=['hola','mundo']

    def __init__(self):
        """Inicializa la clase VentaPaquetes"""
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.iniciarTablas()
        self.ButtonHoteles.clicked.connect(self.consultarHoteles)
        self.ButtonAerolineas.clicked.connect(self.consultarAerolineas)
        self.ButtonRegiones.clicked.connect(self.consultarRegiones)
        self.ButtonPagar.clicked.connect(self.pagar)

    def consultarRegiones(self):
        data=server_agency_remote.get_regions()
        self.tableRegiones.clearContents()
        row = 0
        for dat in data:
            self.tableRegiones.setRowCount(row + 1)

            self.tableRegiones.setItem(row, 0, QTableWidgetItem(str(dat.get('id'))))
            self.tableRegiones.setItem(row, 1, QTableWidgetItem((dat.get('name'))))
            self.tableRegiones.setItem(row, 2, QTableWidgetItem((str(dat.get('cost')))))
            row += 1

    def cargarPaises(self,row,column):
        id=self.tableRegiones.item(row,0).text()
        data=server_agency_remote.get_countrys(id)
        self.tablaPaises.clearContents()
        row = 0
        for dat in data:
            self.tablaPaises.setRowCount(row + 1)

            self.tablaPaises.setItem(row, 0, QTableWidgetItem(str(dat.get('id'))))
            self.tablaPaises.setItem(row, 1, QTableWidgetItem((dat.get('name'))))
            self.tablaPaises.setItem(row, 2, QTableWidgetItem((str(dat.get('cost')))))
            row += 1
        # Activamos boton de hoteles
        self.ButtonHoteles.setEnabled(True)
        self.ButtonAerolineas.setEnabled(True)

    def cargarCiudades(self,row,column):
        id=self.tablaPaises.item(row,0).text()
        data=server_agency_remote.get_citys(id)
        self.tablaCiudades.clearContents()
        row = 0
        for dat in data:
            self.tablaCiudades.setRowCount(row + 1)

            self.tablaCiudades.setItem(row, 0, QTableWidgetItem(str(dat.get('id'))))
            self.tablaCiudades.setItem(row, 1, QTableWidgetItem((dat.get('name'))))
            self.tablaCiudades.setItem(row, 2, QTableWidgetItem((str(dat.get('cost')))))
            row += 1

    def cargarActividades(self,row,column):
        id=self.tablaCiudades.item(row,0).text()
        data=server_agency_remote.get_activitys(id)
        self.tablaActividades.clearContents()
        row = 0
        for dat in data:
            self.tablaActividades.setRowCount(row + 1)

            self.tablaActividades.setItem(row, 0, QTableWidgetItem(str(dat.get('id'))))
            self.tablaActividades.setItem(row, 1, QTableWidgetItem((dat.get('name'))))
            self.tablaActividades.setItem(row, 2, QTableWidgetItem((str(dat.get('cost')))))
            self.tablaActividades.setItem(row, 3, QTableWidgetItem((dat.get('description'))))
            row += 1

    """Por medio del menu contextual se llama este procedimiento
    El procedimiento toma el valor actual de la tabla y lo agrega"""
    def agregarDesdeTablaCiudad(self):
        row_r=self.tableRegiones.currentRow()
        id_r=self.tableRegiones.item(row_r,0).text()

        row_p=self.tablaPaises.currentRow()
        id_p=self.tablaPaises.item(row_p,0).text()

        row_c=self.tablaCiudades.currentRow()
        id_c=self.tablaCiudades.item(row_c,0).text()
        print("-><")
        print(id_r)
        if(seleccion_tabla==1):
            if(id_r not in lista_regiones):
                lista_regiones.append(id_r)
                lista_tem = [self.tableRegiones.item(row_r,1).text(),self.tableRegiones.item(row_r,2).text()]
                resumen['regiones'].append(lista_tem)
            if(id_p not in lista_paises):
                lista_paises.append(id_p)
                lista_tem = [self.tablaPaises.item(row_p,1).text(),self.tablaPaises.item(row_p,2).text()]
                resumen['paises'].append(lista_tem)
            if(id_c not in lista_ciudades):
                lista_ciudades.append(id_c)
                lista_tem = [self.tablaCiudades.item(row_c,1).text(),self.tablaCiudades.item(row_c,2).text()]
                resumen['ciudades'].append(lista_tem)
        if(seleccion_tabla==2):
            if(id_c not in lista_hoteles):
                lista_hoteles.append(id_c)
                # lista_tem = [self.tablaCiudades.item(row_r,1).text(),self.tablaCiudades.item(row_r,2).text()]
                # resumen['hoteles'].append(lista_tem)

    def agregarDesdeTablaActividad(self):
        row_r=self.tableRegiones.currentRow()
        id_r=self.tableRegiones.item(row_r,0).text()

        row_p=self.tablaPaises.currentRow()
        id_p=self.tablaPaises.item(row_p,0).text()

        row_c=self.tablaCiudades.currentRow()
        id_c=self.tablaCiudades.item(row_c,0).text()

        row_a=self.tablaActividades.currentRow()
        id_a=self.tablaActividades.item(row_c,0).text()

        if(seleccion_tabla==1):
            if(id_r not in lista_regiones):
                lista_regiones.append(id_r)
                lista_tem = [self.tableRegiones.item(row_r,1).text(),self.tableRegiones.item(row_r,2).text()]
                resumen['regiones'].append(lista_tem)
            if(id_p not in lista_paises):
                lista_paises.append(id_p)
                lista_tem = [self.tablaPaises.item(row_p,1).text(),self.tablaPaises.item(row_p,2).text()]
                resumen['paises'].append(lista_tem)
            if(id_c not in lista_ciudades):
                lista_ciudades.append(id_c)
                lista_tem = [self.tablaCiudades.item(row_c,1).text(),self.tablaCiudades.item(row_c,2).text()]
                resumen['ciudades'].append(lista_tem)
            if(id_a not in lista_actividades):
                lista_actividades.append(id_c)
                lista_tem = [self.tablaActividades.item(row_a,1).text(),self.tablaActividades.item(row_a,2).text()]
                resumen['actividades'].append(lista_tem)

    def crearPaquete(self):
        print(self.paquete[0])

    def consultarHoteles(self):
        pais="hola"
        data=server_hotel_remote.get_hotels()
        tam=self.tablaCiudades.rowCount()
        print(tam)
        for i in range(tam):
            self.tablaCiudades.removeRow(i)
        # Establecer el número de columnas
        self.tablaCiudades.setColumnCount(4)

        # self.tablaCiudades.verticalHeader().setHighlightSections(True)

        nombreColumnas = ("Id","Nombre","Costo","Descripcion")
        # Establecer las etiquetas de encabezado horizontal usando etiquetas
        self.tablaCiudades.setHorizontalHeaderLabels(nombreColumnas)

        self.tablaCiudades.clearContents()
        row = 0
        for dat in data:
            self.tablaCiudades.setRowCount(row + 1)
            self.tablaCiudades.setItem(row, 0, QTableWidgetItem(str(dat.get('id'))))
            self.tablaCiudades.setItem(row, 1, QTableWidgetItem(dat.get('name')))
            self.tablaCiudades.setItem(row, 2, QTableWidgetItem((str(dat.get('cost')))))
            self.tablaCiudades.setItem(row, 3, QTableWidgetItem(dat.get('description')))
            row += 1
        seleccion_tabla=2

    def consultarAerolineas(self):
        print("Consultar Aerolineas")

    def pagar(self):
        numero_personas=self.spinBoxPersonas.value()
        paquete={'regiones':lista_regiones,'paises':lista_paises,
        'ciudades':lista_ciudades,'actividades':lista_actividades,
        'hoteles':lista_hoteles,'aerolineas':lista_aerolineas}
        resumen['numero_personas']=numero_personas
        self.pago=PagoPaquete(paquete,resumen)
        self.pago.show()
    def iniciarTablas(self):
        """Tabla Regiones """
        # Deshabilitar edición
        self.tableRegiones.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Deshabilitar el comportamiento de arrastrar y soltar
        self.tableRegiones.setDragDropOverwriteMode(False)

        # Seleccionar toda la fila
        self.tableRegiones.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Seleccionar una fila a la vez
        self.tableRegiones.setSelectionMode(QAbstractItemView.SingleSelection)

        # Especifica dónde deben aparecer los puntos suspensivos "..." cuando se muestran
        # textos que no encajan
        self.tableRegiones.setTextElideMode(Qt.ElideRight)# Qt.ElideNone

        # Establecer el ajuste de palabras del texto
        self.tableRegiones.setWordWrap(False)

        # Deshabilitar clasificación
        self.tableRegiones.setSortingEnabled(False)

        # Establecer el número de columnas
        self.tableRegiones.setColumnCount(3)

        # Establecer el número de filas
        self.tableRegiones.setRowCount(0)

        # Alineación del texto del encabezado
        self.tableRegiones.horizontalHeader().setDefaultAlignment(
            Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter
        )

        # Deshabilitar resaltado del texto del encabezado al seleccionar una fila
        self.tableRegiones.horizontalHeader().setHighlightSections(False)

        # Hacer que la última sección visible del encabezado ocupa todo el espacio disponible
        self.tableRegiones.horizontalHeader().setStretchLastSection(True)

        # Ocultar encabezado vertical
        self.tableRegiones.verticalHeader().setVisible(False)

        # Dibujar el fondo usando colores alternados
        self.tableRegiones.setAlternatingRowColors(True)

        # Establecer altura de las filas
        self.tableRegiones.verticalHeader().setDefaultSectionSize(20)

        # self.tableRegiones.verticalHeader().setHighlightSections(True)

        nombreColumnas = ("Id","Nombre","Costo")

        # Establecer las etiquetas de encabezado horizontal usando etiquetas
        self.tableRegiones.setHorizontalHeaderLabels(nombreColumnas)

        # Menú contextual
        #self.tableRegiones.setContextMenuPolicy(Qt.CustomContextMenu)
        #self.tableRegiones.customContextMenuRequested.connect(self.menuContextual)

        # Establecer ancho de las columnas
        for indice, ancho in enumerate((20, 120), start=0):
            self.tableRegiones.setColumnWidth(indice, ancho)

        self.tableRegiones.resize(700, 240)
        self.tableRegiones.move(20, 56)

        #menu.triggered.connect(self.mostrarOcultar)
        self.tableRegiones.cellClicked.connect(self.cargarPaises)

        """Tabla Paises """
        self.tablaPaises.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Deshabilitar el comportamiento de arrastrar y soltar
        self.tablaPaises.setDragDropOverwriteMode(False)

        # Seleccionar toda la fila
        self.tablaPaises.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Seleccionar una fila a la vez
        self.tablaPaises.setSelectionMode(QAbstractItemView.SingleSelection)

        # Especifica dónde deben aparecer los puntos suspensivos "..." cuando se muestran
        # textos que no encajan
        self.tablaPaises.setTextElideMode(Qt.ElideRight)# Qt.ElideNone

        # Establecer el ajuste de palabras del texto
        self.tablaPaises.setWordWrap(False)

        # Deshabilitar clasificación
        self.tablaPaises.setSortingEnabled(False)

        # Establecer el número de columnas
        self.tablaPaises.setColumnCount(3)

        # Establecer el número de filas
        self.tablaPaises.setRowCount(0)

        # Alineación del texto del encabezado
        self.tablaPaises.horizontalHeader().setDefaultAlignment(
            Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter
        )

        # Deshabilitar resaltado del texto del encabezado al seleccionar una fila
        self.tablaPaises.horizontalHeader().setHighlightSections(False)

        # Hacer que la última sección visible del encabezado ocupa todo el espacio disponible
        self.tablaPaises.horizontalHeader().setStretchLastSection(True)

        # Ocultar encabezado vertical
        self.tablaPaises.verticalHeader().setVisible(False)

        # Dibujar el fondo usando colores alternados
        self.tablaPaises.setAlternatingRowColors(True)

        # Establecer altura de las filas
        self.tablaPaises.verticalHeader().setDefaultSectionSize(20)

        # self.tablaPaises.verticalHeader().setHighlightSections(True)

        nombreColumnas = ("Id","Nombre","Costo")

        # Establecer las etiquetas de encabezado horizontal usando etiquetas
        self.tablaPaises.setHorizontalHeaderLabels(nombreColumnas)

        # Menú contextual
        # self.tablaPaises.setContextMenuPolicy(Qt.CustomContextMenu)
        # self.tablaPaises.customContextMenuRequested.connect(self.menuContextual)

        # Establecer ancho de las columnas
        for indice, ancho in enumerate((20, 120), start=0):
            self.tablaPaises.setColumnWidth(indice, ancho)

        self.tablaPaises.resize(700, 240)
        self.tablaPaises.move(20, 56)

        self.tablaPaises.cellClicked.connect(self.cargarCiudades)

        """Tabla Ciudades """
        self.tablaCiudades.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Deshabilitar el comportamiento de arrastrar y soltar
        self.tablaCiudades.setDragDropOverwriteMode(False)

        # Seleccionar toda la fila
        self.tablaCiudades.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Seleccionar una fila a la vez
        self.tablaCiudades.setSelectionMode(QAbstractItemView.SingleSelection)

        # Especifica dónde deben aparecer los puntos suspensivos "..." cuando se muestran
        # textos que no encajan
        self.tablaCiudades.setTextElideMode(Qt.ElideRight)# Qt.ElideNone

        # Establecer el ajuste de palabras del texto
        self.tablaCiudades.setWordWrap(False)

        # Deshabilitar clasificación
        self.tablaCiudades.setSortingEnabled(False)

        # Establecer el número de columnas
        self.tablaCiudades.setColumnCount(3)

        # Establecer el número de filas
        self.tablaCiudades.setRowCount(0)

        # Alineación del texto del encabezado
        self.tablaCiudades.horizontalHeader().setDefaultAlignment(
            Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter
        )

        # Deshabilitar resaltado del texto del encabezado al seleccionar una fila
        self.tablaCiudades.horizontalHeader().setHighlightSections(False)

        # Hacer que la última sección visible del encabezado ocupa todo el espacio disponible
        self.tablaCiudades.horizontalHeader().setStretchLastSection(True)

        # Ocultar encabezado vertical
        self.tablaCiudades.verticalHeader().setVisible(False)

        # Dibujar el fondo usando colores alternados
        self.tablaCiudades.setAlternatingRowColors(True)

        # Establecer altura de las filas
        self.tablaCiudades.verticalHeader().setDefaultSectionSize(20)

        # self.tablaCiudades.verticalHeader().setHighlightSections(True)

        nombreColumnas = ("Id","Nombre","Costo")

        # Establecer las etiquetas de encabezado horizontal usando etiquetas
        self.tablaCiudades.setHorizontalHeaderLabels(nombreColumnas)

        # Menú contextual
        self.tablaCiudades.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tablaCiudades.customContextMenuRequested.connect(self.menuContextual)

        # Establecer ancho de las columnas
        for indice, ancho in enumerate((60, 120), start=0):
            self.tablaCiudades.setColumnWidth(indice, ancho)

        self.tablaCiudades.resize(700, 240)
        self.tablaCiudades.move(20, 56)
        self.tablaCiudades.cellClicked.connect(self.cargarActividades)

        """Tabla Actividades"""

        self.tablaActividades.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Deshabilitar el comportamiento de arrastrar y soltar
        self.tablaActividades.setDragDropOverwriteMode(False)

        # Seleccionar toda la fila
        self.tablaActividades.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Seleccionar una fila a la vez
        self.tablaActividades.setSelectionMode(QAbstractItemView.SingleSelection)

        # Especifica dónde deben aparecer los puntos suspensivos "..." cuando se muestran
        # textos que no encajan
        self.tablaActividades.setTextElideMode(Qt.ElideRight)# Qt.ElideNone

        # Establecer el ajuste de palabras del texto
        self.tablaActividades.setWordWrap(False)

        # Deshabilitar clasificación
        self.tablaActividades.setSortingEnabled(False)

        # Establecer el número de columnas
        self.tablaActividades.setColumnCount(4)

        # Establecer el número de filas
        self.tablaActividades.setRowCount(0)

        # Alineación del texto del encabezado
        self.tablaActividades.horizontalHeader().setDefaultAlignment(
            Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter
        )

        # Deshabilitar resaltado del texto del encabezado al seleccionar una fila
        self.tablaActividades.horizontalHeader().setHighlightSections(False)

        # Hacer que la última sección visible del encabezado ocupa todo el espacio disponible
        self.tablaActividades.horizontalHeader().setStretchLastSection(True)

        # Ocultar encabezado vertical
        self.tablaActividades.verticalHeader().setVisible(False)

        # Dibujar el fondo usando colores alternados
        self.tablaActividades.setAlternatingRowColors(True)

        # Establecer altura de las filas
        self.tablaActividades.verticalHeader().setDefaultSectionSize(20)

        # self.tablaActividades.verticalHeader().setHighlightSections(True)

        nombreColumnas = ("Id","Nombre","Costo","Descripcion")

        # Establecer las etiquetas de encabezado horizontal usando etiquetas
        self.tablaActividades.setHorizontalHeaderLabels(nombreColumnas)

         # Menú contextual
        self.tablaActividades.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tablaActividades.customContextMenuRequested.connect(self.menuContextualActividades)

        # Establecer ancho de las columnas
        for indice, ancho in enumerate((60, 120), start=0):
            self.tablaActividades.setColumnWidth(indice, ancho)

        self.tablaActividades.resize(700, 240)
        self.tablaActividades.move(20, 56)

    def menuContextual(self, posicion):
        indices = self.tablaCiudades.selectedIndexes()

        if indices:
            menu = QMenu()

            itemsGrupo = QActionGroup(self)
            itemsGrupo.setExclusive(True)

            menu.addAction(QAction(" Añadir ", itemsGrupo))

            columnas = [self.tablaCiudades.horizontalHeaderItem(columna).text()
                        for columna in range(self.tablaCiudades.columnCount())
                        if not self.tablaCiudades.isColumnHidden(columna)]

            itemsGrupo.triggered.connect(self.agregarDesdeTablaCiudad)

            menu.exec_(self.tablaCiudades.viewport().mapToGlobal(posicion))

    def menuContextualActividades(self, posicion):
        indices = self.tablaActividades.selectedIndexes()

        if indices:
            menu = QMenu()

            itemsGrupo = QActionGroup(self)
            itemsGrupo.setExclusive(True)

            menu.addAction(QAction(" Añadir ", itemsGrupo))

            columnas = [self.tablaActividades.horizontalHeaderItem(columna).text()
                        for columna in range(self.tablaActividades.columnCount())
                        if not self.tablaActividades.isColumnHidden(columna)]

            itemsGrupo.triggered.connect(self.agregarDesdeTablaActividad)

            menu.exec_(self.tablaActividades.viewport().mapToGlobal(posicion))


if __name__ == "__main__":
    """Funcion principal"""
    app =  QtWidgets.QApplication(sys.argv)
    window = VentaPaquetes()
    window.show()
    sys.exit(app.exec_())
