import sys
import Pyro4
from resumen import Resumen
from time import gmtime, strftime
from PyQt5 import uic, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QIcon, QColor
from PyQt5.QtWidgets import (QApplication, QDialog, QPushButton, QTableWidget,
                             QTableWidgetItem, QAbstractItemView, QHeaderView, QMenu,
                             QActionGroup, QAction, QMessageBox)

# Nombre del archivo aquí
qtCreatorFile = "pago_paquete.ui"

name_server = Pyro4.locateNS()
server_agency_uri = name_server.lookup('server_agency')
server_agency_remote = Pyro4.Proxy(server_agency_uri)

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class PagoPaquete(QtWidgets.QMainWindow, Ui_MainWindow):
    """Clase PagoPaquete"""

    def __init__(self,paquete,resumen):
        """Inicializa la clase PagoPaquete"""
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.res=resumen
        self.paq=paquete
        self.setupUi(self)
        self.llenarDatos()
        self.ButtonComprar.clicked.connect(self.comprar)
        self.ButtonResumen.clicked.connect(self.resumen)

    def llenarDatos(self):
        self.sumarCostos()
        data=server_agency_remote.get_clients()

        self.lineEditTipo.setText('Personalizado')
        self.lineEditFecha.setText(str(strftime("%a, %d %b %Y", gmtime())))
        self.lineEditFechaSalida.setText('---')
        self.lineEditNumeroPersonas.setText(str(self.res['numero_personas']))
        self.tablaClientes.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Deshabilitar el comportamiento de arrastrar y soltar
        self.tablaClientes.setDragDropOverwriteMode(False)

        # Seleccionar toda la fila
        self.tablaClientes.setSelectionBehavior(QAbstractItemView.SelectRows)

        # Seleccionar una fila a la vez
        self.tablaClientes.setSelectionMode(QAbstractItemView.SingleSelection)

        # Especifica dónde deben aparecer los puntos suspensivos "..." cuando se muestran
        # textos que no encajan
        self.tablaClientes.setTextElideMode(Qt.ElideRight)# Qt.ElideNone

        # Establecer el ajuste de palabras del texto
        self.tablaClientes.setWordWrap(False)

        # Deshabilitar clasificación
        self.tablaClientes.setSortingEnabled(False)

        # Establecer el número de columnas
        self.tablaClientes.setColumnCount(2)

        # Establecer el número de filas
        self.tablaClientes.setRowCount(0)

        # Alineación del texto del encabezado
        self.tablaClientes.horizontalHeader().setDefaultAlignment(
            Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter
        )

        # Deshabilitar resaltado del texto del encabezado al seleccionar una fila
        self.tablaClientes.horizontalHeader().setHighlightSections(False)

        # Hacer que la última sección visible del encabezado ocupa todo el espacio disponible
        self.tablaClientes.horizontalHeader().setStretchLastSection(True)

        # Ocultar encabezado vertical
        self.tablaClientes.verticalHeader().setVisible(False)

        # Dibujar el fondo usando colores alternados
        self.tablaClientes.setAlternatingRowColors(True)

        # Establecer altura de las filas
        self.tablaClientes.verticalHeader().setDefaultSectionSize(20)

        # self.tablaClientes.verticalHeader().setHighlightSections(True)

        nombreColumnas = ("Cedula","Nombre")

        # Establecer las etiquetas de encabezado horizontal usando etiquetas
        self.tablaClientes.setHorizontalHeaderLabels(nombreColumnas)

        # Menú contextual
        # self.tablaClientes.setContextMenuPolicy(Qt.CustomContextMenu)
        # self.tablaClientes.customContextMenuRequested.connect(self.menuContextual)

        # Establecer ancho de las columnas
        for indice, ancho in enumerate((80, 120), start=0):
            self.tablaClientes.setColumnWidth(indice, ancho)

        self.tablaClientes.resize(700, 240)
        self.tablaClientes.move(20, 56)
        self.tablaClientes.clearContents()

        self.tablaClientes.cellClicked.connect(self.seleccionarCliente)
        row = 0
        for cliente in data:
            self.tablaClientes.setRowCount(row + 1)
            self.tablaClientes.setItem(row, 0, QTableWidgetItem(str(cliente.get('id'))))
            self.tablaClientes.setItem(row, 1, QTableWidgetItem((cliente.get('names'))))
            row += 1

    def resumen(self):
        self.Resumen=Resumen(self.res)
        self.Resumen.show()

    def seleccionarCliente(self,row,column):
        id=self.tablaClientes.item(row,0).text()
        cliente=server_agency_remote.get_client_by_id(id)
        self.lineEditNombre.setText(cliente.get('names')+ " " +str(cliente.get('last_names')))
        self.lineEditDNI.setText(str(cliente.get('id')))
        self.lineEditTelefono.setText(str(cliente.get('telephone')))
        self.lineEditDireccion.setText(str(cliente.get('address')))
        self.ButtonComprar.setEnabled(True)
        print(cliente)

    def comprar(self):
        """Concatenacion de Costs de Agencia"""
        regiones=self.res['regiones']
        region=''
        for reg in regiones:
            region+=(reg[0])+', '

        paises=self.res['paises']
        pais=''
        for reg in paises:
            pais+=(reg[0])+', '

        ciudades=self.res['ciudades']
        ciudad=''
        for reg in ciudades:
            ciudad+=(reg[0])+', '

        actividades=self.res['actividades']
        actividad=''
        for reg in actividades:
            actividad+=(reg[0])+', '

        """Suma Costos Hoteles"""
        hoteles=self.res['hoteles']
        hotel=''
        for reg in hoteles:
            hotel+=(reg[0])+', '

        """suma Costos Aerolineas"""
        aerolineas=self.res['aerolineas']
        aerolinea=''
        for reg in aerolineas:
            aerolinea+=(reg[0])+', '

        client_id=self.lineEditDNI.text()
        fecha=self.lineEditFecha.text()
        number_people=self.lineEditNumeroPersonas.text()
        cost=self.lineEditCostos.text()
        package={'cost':cost,'date':fecha,'number_people':number_people,'client_id':client_id,
        'regions':region,'countrys':pais,'citys':ciudad,'activitys':actividad,
        'hotels':hotel,'airlines':aerolinea}
        ##Invocacion del metodo remoto de registro del paquete
        server_agency_remote.create_package(package)
        msgBox=QMessageBox()
        msgBox.setText("Se registro la venta correctamenete")
        msgBox.exec()

    def sumarCostos(self):
        """Suma Costos Agencia"""
        sum_total=0
        regiones=self.res['regiones']
        suma=0
        for reg in regiones:
            suma+=int(reg[1])

        paises=self.res['paises']

        for reg in paises:
            suma+=int(reg[1])

        ciudades=self.res['ciudades']

        for reg in ciudades:
            suma+=int(reg[1])

        actividades=self.res['actividades']

        for reg in actividades:
            suma+=int(reg[1])

        self.lineEditCostos.setText(str(suma))
        sum_total+=suma
        """Suma Costos Hoteles"""
        hoteles=self.res['hoteles']
        suma=0
        for reg in hoteles:
            suma+=int(reg[1])

        self.lineEditHoteles.setText(str(suma))
        """suma Costos Aerolineas"""
        aerolineas=self.res['aerolineas']
        sum_total+=suma
        suma=0
        for reg in aerolineas:
            suma+=int(reg[1])
        self.lineEditAerolineas.setText(str(suma))
        num_pers=self.res['numero_personas']
        sum_total+=suma
        self.lineEditTotal.setText(str(num_pers*sum_total))


if __name__ == "__main__":
    """Funcion principal"""
    app =  QtWidgets.QApplication(sys.argv)
    window = PagoPaquete()
    window.show()
    sys.exit(app.exec_())
