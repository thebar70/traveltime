import sys
from PyQt5 import uic, QtWidgets

# Nombre del archivo aquí
qtCreatorFile = "resumen.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class Informacion(QtWidgets.QMainWindow, Ui_MainWindow):
    """Clase Informacion"""

    def __init__(self,resumen):
        """Inicializa la clase Informacion"""
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.res=resumen
        self.setupUi(self)
        self.llenarDatos()
        self.ButtonAceptar.clicked.connect(self.cerrar)

    def llenarDatos(self):
        """Suma Costos Agencia"""
        regiones=self.res.get('regions')
        paises=self.res.get('countrys')
        ciudades=self.res.get('citys')
        actividades=self.res.get('activitys')

        """Suma Costos Hoteles"""
        hoteles=self.res.get('hotels')

        """suma Costos Aerolineas"""
        aerolineas=self.res.get('airlines')

        numero_personas=self.res.get('number_people')
        self.lineEditRegiones.setText(regiones)
        self.lineEditPaises.setText(paises)
        self.lineEditCiudades.setText(ciudades)
        self.lineEditActividades.setText(actividades)
        self.lineEditHoteles.setText(hoteles)
        self.lineEditAerolineas.setText(aerolineas)
        self.lineEditPersonas.setText(str(numero_personas))
        self.lineEditFechaSalida.setText(self.res.get('date'))

    def cerrar(self):
        print("cerrar")


if __name__ == "__main__":
    """Funcion principal"""
    app =  QtWidgets.QApplication(sys.argv)
    window = Informacion()
    window.show()
    sys.exit(app.exec_())
