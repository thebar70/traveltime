import Pyro4
import json
import sys
from PyQt5 import uic, QtWidgets
from PyQt5.QtWidgets import QMessageBox

# Nombre del archivo aquí
qtCreatorFile = "registrar_usuario.ui"
name_server = Pyro4.locateNS()
server_agency_uri = name_server.lookup('server_agency')
server_agency_remote = Pyro4.Proxy(server_agency_uri)

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class RegistarUsuario(QtWidgets.QMainWindow, Ui_MainWindow):
    """Clase RegistarUsuario"""

    def __init__(self):
        """Inicializa la clase RegistarUsuario"""
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.comboBoxGenero.addItem('M')
        self.comboBoxGenero.addItem('F')
        self.ButtonRegistrar.clicked.connect(self.registrar_usuario)

    def registrar_usuario(self):
        """Registar usuario
        Obtiene los datos del formulario y crea el diccionario cliente
        Con el diccionario cliente creado, invoca el metodo remoto <create_client>
        Se pasa como parametro el clinte a registar
        """
        nombres=self.lineEditNombres.text()
        apellidos=self.lineEditApellidos.text()
        direccion=self.lineEditDireccion.text()
        ciudad=self.lineEditCiudad.text()
        telefono=self.lineEditTelefono.text()
        cedula=self.lineEditCedula.text()
        email=self.lineEditEmail.text()
        genero=self.comboBoxGenero.currentText()
        fechaNacimiento=self.calendar.selectedDate().toString('dd/MM/yyyy')
        cliente={'id': cedula,'names':nombres,'last_names':apellidos,'birthdate':fechaNacimiento,
        'email':email,'gender':genero,'address':direccion,'city_residence':ciudad,'telephone':telefono}
        server_agency_remote.create_client(cliente)
        msgBox=QMessageBox()
        msgBox.setText("Se registro el cliente correctamente")
        msgBox.exec()
        self.limpiar()

    def limpiar(self):
        """Limpia el contenido de la pantalla una vez se ha registrado
        el cliente
        """
        self.lineEditNombres.setText('')
        self.lineEditApellidos.setText('')
        self.lineEditDireccion.setText('')
        self.lineEditCiudad.setText('')
        self.lineEditTelefono.setText('')
        self.lineEditCedula.setText('')
        self.lineEditEmail.setText('')


if __name__ == "__main__":
    """Funcion principal"""
    app =  QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
