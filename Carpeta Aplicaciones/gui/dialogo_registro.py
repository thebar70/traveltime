import sys
from PyQt5 import uic, QtWidgets

# Nombre del archivo aquí
qtCreatorFile = "dialogo_registro.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):
    """Clase MyApp"""

    def __init__(self):
        """Inicializa la clase MyApp"""
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.pushButtonAceptar.clicked.connect(self.cerrar)

    def cerrar(self):
        """Cierra la ventana"""
        sys.exit(app.exec_())


if __name__ == "__main__":
    """Funcion principal"""
    app =  QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
