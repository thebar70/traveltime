import sys
from venta_paquetes import VentaPaquetes
from registrar_usuario import RegistarUsuario
from buscar_paquetes import BuscarPaquetes
from PyQt5 import uic, QtWidgets
from PyQt5.QtGui import QFont, QIcon, QColor, QImage
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QApplication, QDialog, QPushButton, QTableWidget,
                             QTableWidgetItem, QAbstractItemView, QHeaderView, QMenu,
                             QActionGroup, QAction, QMessageBox, QMdiArea,QMdiSubWindow)

# Nombre del archivo aquí
qtCreatorFile = "ventana_principal.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):
    """Clase MyApp"""

    def __init__(self):
        """Inicializa la clase MyApp"""
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.iniciar_sesion()
        # img=QImage("resources/fondo.jpg")
        # self.mdiArea.setBackground(img)
        bar = self.menuBar()
        file = bar.addMenu("Archivo")
        edit = bar.addMenu("Editar")
        opciones = bar.addMenu("Opciones")
        ayuda = bar.addMenu("ayuda")

        file.addMenu("Nueva ventana")
        file.addMenu("Abrir Archivo")
        file.addMenu("Guardar")
        file.addMenu("Salir")

        edit.addMenu("Limpiar")
        edit.addMenu("Cortar")
        edit.addMenu("Copiar")
        edit.addMenu("Pegar")

        clientes = opciones.addMenu("Clientes")
        paquetes = opciones.addMenu("Paquetes")
        hoteltes = opciones.addMenu("Hoteles")
        aerolineas = opciones.addMenu("Aerolineas")

        registrar=clientes.addMenu("Registrar Clientes")
        clientes.addMenu("Concultar Cliente")
        clientes.addMenu("Listar Clientes")

        personalizados = paquetes.addMenu("Paquetes Personalizados")
        paquetes.addMenu("Paquetes En Promocion")
        busqueda=paquetes.addMenu("Paquetes por Cliente")

        hotel=hoteltes.addMenu("Consultar Hotel")
        hoteltes.addMenu("Listar Hoteles")

        aerolineas.addMenu("Consultar Aerolinea")
        aerolineas.addMenu("Listar Aerolineas")

        # Acciones
        personalizados.addAction("Nuevo")
        personalizados.triggered[QAction].connect(self.ventaPaquetesPersonalizados)

        hotel.addAction("Ver")
        hotel.triggered[QAction].connect(self.consultarHoteles)

        registrar.addAction("Registrar")
        registrar.triggered[QAction].connect(self.registrar_clientes)

        busqueda.addAction("Buscar")
        busqueda.triggered[QAction].connect(self.buscar_paquetes)
        """
        save = QAction("Save",self)
        file.addAction("New")
        save = QAction("Save",self)
        save.setShortcut("Ctrl+S")
        file.addAction(save)

        edit = file.addMenu("Edit")
        edit.addAction("copy")
        edit.addAction("paste")

        quit = QAction("Quit",self)
        file.addAction(quit)
        file.triggered[QAction].connect(self.processtrigger)"""

    def ventaPaquetesPersonalizados(self):
        ventana=VentaPaquetes()
        sub=QMdiSubWindow()
        sub.setWidget(ventana)
        self.mdiArea.addSubWindow(sub)
        sub.show()
        # self.VentaPaquetes.setupUi(self)

    def consultarHoteles(self):
        ventana=SeleccionHotel()
        sub=QMdiSubWindow()
        sub.setWidget(ventana)
        self.mdiArea.addSubWindow(sub)
        sub.show()

    def registrar_clientes(self):
        ventana=RegistarUsuario()
        sub=QMdiSubWindow()
        sub.setWidget(ventana)
        self.mdiArea.addSubWindow(sub)
        sub.show()

    def buscar_paquetes(self):
        ventana=BuscarPaquetes()
        sub=QMdiSubWindow()
        sub.setWidget(ventana)
        self.mdiArea.addSubWindow(sub)
        sub.show()

    def cerrar(self):
        """Cerrar ventana"""
        sys.exit(app.exec_())

    def iniciar_sesion(self):
        self.Login= Login()
        self.Login.exec_()


class Login(QDialog):
    """Clase Login"""

    def __init__(self):
        """Inicializa la clase Login"""
        QDialog.__init__(self)
        uic.loadUi('login.ui',self)


if __name__ == "__main__":
    """Funcion principal"""
    app =  QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
