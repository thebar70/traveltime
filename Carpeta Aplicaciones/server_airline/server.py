import Pyro4

from controllers.controllers import ControllerAirline
from controllers.controllers import ControllerFlight

# Config server airline


@Pyro4.expose
class ServerAirline:
    """Clase Servidor Aerolinea"""

    def __init__(self):
        """Inicializa la clase Servidor Aerolinea
        asigna:
            instancia de Controlador Aerolinea
            instancia de Controlador Vuelo
        """
        self.controller_airline = ControllerAirline()
        self.controller_flight = ControllerFlight()

    def create_airline(self, dic):
        """Crea una aerolinea
        parametros:
            diccionario con los datos de aerolinea
        retorna:
            Booleano
        """
        return self.controller_airline.create_airline(dic)

    def create_flight(self, dic):
        """Crea una vuelo
        parametros:
            diccionario con los datos de vuelo
        retorna:
            Booleano
        """
        return self.controller_flight.create_flight(dic)

    def update_airline(self, dic):
        """Actualiza una aerolinea
        parametros:
            diccionario con los datos de aerolinea
        retorna:
            Booleano
        """
        return self.controller_airline.update_airline(dic)

    def delete_airline(self, id):
        """Elimina una aerolinea
        parametros:
            id de la aerolinea
        retorna:
            Booleano
        """
        return self.controller_airline.delete_airline(id)

    def get_airline_by_id(self, id):
        """Recupera una aerolinea
        parametros:
            id de la aerolinea
        retorna:
            diccionario con los datos de la aerolinea
        """
        return self.controller_airline.get_airline_by_id(id)

    def get_airline_by_name(self, name):
        """Recupera una aerolinea
        parametros:
            nombre de la aerolinea
        retorna:
            diccionario con los datos de la aerolinea
        """
        return self.controller_airline.get_airline_by_name(name)

    def get_airlines(self):
        """Recupera todos las aerolineas
        retorna:
            lista de diccionarios con los datos de todas las aerolineas
        """
        return self.controller_airline.get_airlines()

    def get_flights(self):
        """Recupera todos los vuelos
        retorna:
            lista de diccionarios con los datos de todas los vuelos
        """
        return self.controller_flight.get_flights()


def run():
    """Metodo que registra la clase Servidor Aerolinea
    en el demonio de Pyro4
    """
    objects = {
        ServerAirline: 'server_airline'
    }
    Pyro4.Daemon.serveSimple(objects)


if __name__ == '__main__':
    run()
