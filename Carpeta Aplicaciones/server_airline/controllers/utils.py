from models.models import Airline
from models.models import Flight

# Methods utilities


def transform_dict_to_airline(dic):
    """Transforma un diccionario a aerolinea
    parametros:
        diccionario con los datos de aerolinea
    retorna:
        instancia de aerolinea
    """
    for key, value in dic.items():
        if isinstance(value, str):
            dic.update({key: value.lower()})
    return Airline(**dic)

def transform_dict_to_flight(dic):
    """Transforma un diccionario a vuelo
    parametros:
        instancia de vuelo
    retorna:
        diccionario con los datos de vuelo
    """
    for key, value in dic.items():
        if isinstance(value, str):
            dic.update({key: value.lower()})
    return Flight(**dic)

def transform_airline_to_dict(airline):
    """Transforma una aerolinea a diccionario
    parametros:
        instancia de aerolinea
    retorna:
        diccionario con los datos de aerolinea
    """
    if not airline:
        return None
    dic = {
        'id': airline.id,
        'name': airline.name,
        'flights': [
            transform_flight_to_dict(flight)
            for flight in airline.flights
        ]
    }
    return dic

def transform_flight_to_dict(flight):
    """Transforma un vuelo a diccionario
    parametros:
        instancia de vuelo
    retorna:
        diccionario con los datos de vuelo
    """
    if not flight:
        return None
    dic = {
        'id': flight.id,
        'origin': flight.origin,
        'destination': flight.destination,
        'date': flight.date,
        'cost': flight.cost,
    }
    return dic

def transform_airlines_to_list(airlines):
    """Agrupa las aerolineas a una lista
    parametros:
        lista de registros de aerolineas
    retorna:
        lista de diccionarios con los datos de aerolineas
    """
    list_airlines = []
    for airline in airlines:
        list_airlines.append(transform_airline_to_dict(airline))
    return list_airlines

def transform_flights_to_list(flights):
    """Agrupa los vuelos a una lista
    parametros:
        lista de registros de vuelos
    retorna:
        lista de diccionarios con los datos de vuelos
    """
    list_flights = []
    for flight in flights:
        list_flights.append(transform_flight_to_dict(flight))
    return list_flights
