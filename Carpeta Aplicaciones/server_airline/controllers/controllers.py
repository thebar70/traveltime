from .utils import transform_airline_to_dict
from .utils import transform_airlines_to_list
from .utils import transform_dict_to_airline
from .utils import transform_flight_to_dict
from .utils import transform_flights_to_list
from .utils import transform_dict_to_flight
from persistence.database import DataBase

# Create yours controllers here


class ControllerAirline:
    """Clase Controlador Aerolinea"""

    def __init__(self):
        """Inicializa la clase Controlador Aerolinea
        asigna:
            instancia de la base de datos
        """
        self.db = DataBase()

    def create_airline(self, dic):
        """Crea una aerolinea
        parametros:
            diccionario con los datos de aerolinea
        retorna:
            Booleano
        """
        airline = transform_dict_to_airline(dic)
        self.db.create_airline(airline)
        return True

    def update_airline(self, dic):
        """Actualiza una aerolinea
        parametros:
            diccionario con los datos de aerolinea
        retorna:
            Booleano
        """
        id = dic.pop('id')
        airline = transform_dict_to_airline(dic)
        self.db.update_airline(id, airline)
        return True

    def delete_airline(self, id):
        """Elimina una aerolinea
        parametros:
            id de la aerolinea
        """
        self.db.delete_airline(id)

    def get_airline_by_id(self, id):
        """Crea una aerolinea
        parametros:
            diccionario con los datos de aerolinea
        retorna:
            Booleano
        """
        airline = self.db.get_airline_by_id(id)
        return transform_airline_to_dict(airline)

    def get_airline_by_name(self, name):
        """Recupera una aerolinea
        parametros:
            nombre de la aerolinea
        retorna:
            diccionario con los datos de aerolinea
        """
        airline = self.db.get_airline_by_name(name.lower())
        return transform_airline_to_dict(airline)

    def get_airlines(self):
        """Recupera todas las aerolineas
        retorna:
            lista de diccionarios con los datos de las aerolineas
        """
        airlines = self.db.get_airlines()
        return transform_airlines_to_list(airlines)


class ControllerFlight:
    """Clase Controlador Vuelo"""

    def __init__(self):
        """Inicializa la clase Controlador Vuelo
        asigna una instancia de la base de datos
        """
        self.db = DataBase()

    def create_flight(self, dic):
        """Crea un vuelo
        parametros:
            diccionario con los datos de vuelo
        """
        id_airline = dic.pop('id_airline')
        flight = transform_dict_to_flight(dic)
        self.db.create_flight(id_airline, flight)

    def get_flights(self):
        """Recupera todos los vuelos
        retorna:
            lista de diccionarios con los datos de vuelos
        """
        flights = self.db.get_flights()
        return transform_flights_to_list(flights)
