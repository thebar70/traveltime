
# Create here yours models


class Airline:
    """Clase Aerolinea"""

    def __init__(self, **kwargs):
        """Inicializa la clase Aerolinea
        atributos:
            name
            flights
        """
        self.name = kwargs.get('name')
        self.flights = []


class Flight:
    """Clase Vuelo"""

    def __init__(self, **kwargs):
        """Inicializa la clase Aerolinea
        atributos:
            origin
            destination
            date
            cost
            airline
        """
        self.origin = kwargs.get('origin')
        self.destination = kwargs.get('destination')
        self.date = kwargs.get('date')
        self.cost = kwargs.get('cost')
        self.airline = None
