import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool
from sqlalchemy.ext.declarative import declarative_base


BASE_DIR = os.getcwd()
Base = declarative_base()
DATABASE = os.path.join(BASE_DIR, 'db_airline.sqlite')

from .models import TableFlight
from .models import TableAirline


class DataBase():
    """Clase encargada de la manipulacion de la base de datos"""

    def __init__(self):
        """Inicializa la clase DataBase
        define el motor y habilita el uso de hilos para la conexión
        """
        engine = create_engine(
            'sqlite:////' + DATABASE,
            echo=False,
            connect_args={'check_same_thread':False},
            poolclass=StaticPool
        )
        Base.metadata.create_all(engine)
        session = sessionmaker(bind=engine)
        self.session = session()

    def create_airline(self, airline):
        """Crear un registro aerolinea
        parametros:
            instancia aerolinea
        """
        airline = TableAirline(**vars(airline))
        self.session.add(airline)
        self.session.commit()
        self.session.close()

    def create_flight(self, id_airline, flight):
        """Crear un registro vuelo
        parametros:
            id de aerolinea
            instancia vuelo
        """
        airline = self.get_airline_by_id(id_airline)
        flight = TableFlight(**vars(flight))
        flight.airline = airline
        airline.flights.append(flight)
        self.session.add(flight)
        self.session.commit()
        self.session.close()

    def update_airline(self, id, airline):
        """Crear un registro aerolinea
        parametros:
            id de aerolinea
            instancia aerolinea
        """
        self.session.query(TableAirline).filter_by(id=id).update(vars(airline))
        self.session.commit()
        self.session.close()

    def delete_airline(self, id):
        """Elimina un registro aerolinea
        parametros:
            id de aerolinea
        """
        self.session.query(TableAirline).filter_by(id=id).delete()
        self.session.commit()
        self.session.close()

    def get_airline_by_id(self, id):
        """Recupera un registro aerolinea
        parametros:
            id de aerolinea
        retorna:
            registro aerolinea
        """
        airline = self.session.query(TableAirline).filter_by(id=id).first()
        self.session.close()
        return airline

    def get_airline_by_name(self, name):
        """Recupera un registro aerolinea
        parametros:
            nombre de aerolinea
        retorna:
            registro aerolinea
        """
        airline = self.session.query(TableAirline).filter_by(name=name).first()
        self.session.close()
        return airline

    def get_airlines(self):
        """Recupera todos los registros aerolineas
        retorna:
            lista de registros de aerolineas
        """
        airlines = self.session.query(TableAirline).all()
        self.session.close()
        return airlines

    def get_flights(self):
        """Recupera todos los registros vuelos
        retorna:
            lista de registros de vuelos
        """
        flights = self.session.query(TableFlight).all()
        self.session.close()
        return flights
