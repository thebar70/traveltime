from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from .database import Base

# Create yours models here


class TableFlight(Base):
    """Clase Vuelo que es mapeada a la base de datos.
    campos:
        id
        origin
        destination
        date
        cost
        airline_id
    """

    __tablename__ = 'flight'

    id = Column(Integer, primary_key=True)
    origin = Column(String(length=45), nullable=False)
    destination = Column(String(length=45), nullable=False)
    date = Column(DateTime, nullable=False)
    cost = Column(Integer, nullable=True)
    airline_id = Column(Integer, ForeignKey('airline.id'))
    airline = relationship('TableAirline', back_populates='flights')

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.origin


class TableAirline(Base):
    """Clase Aerolinea que es mapeada a la base de datos.
    campos:
        id
        name
    """

    __tablename__ = 'airline'

    id = Column(Integer, primary_key=True)
    name = Column(String(length=45), nullable=False)
    flights = relationship('TableFlight', back_populates='airline')

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.name
