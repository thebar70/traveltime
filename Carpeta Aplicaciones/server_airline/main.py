from datetime import datetime

from models.models import Airline
from models.models import Flight
from controllers.controllers import ControllerAirline
from controllers.controllers import ControllerFlight


if __name__ == '__main__':
    """Funcion principal"""

    controller_airline = ControllerAirline()
    controller_flight = ControllerFlight()

    airline = Airline(name='Avianca')

    dic_flight = {
        'origin': 'Cali',
        'destination': 'Medellin',
        'date': datetime.now(),
        'cost': 120000,
        'id_airline': 1
    }

    controller_airline.create_airline(vars(airline))
    controller_flight.create_flight(dic_flight)
    print(controller_airline.get_airlines())
