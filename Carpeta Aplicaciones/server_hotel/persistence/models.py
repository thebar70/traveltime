from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text

from .database import Base

# Create yours models here


class TableHotel(Base):
    """Clase Hotel que es mapeada a la base de datos.
    campos:
        id
        name
        type_hotel
        description
        cost
    """

    __tablename__ = 'hotel'

    id = Column(Integer, primary_key=True)
    name = Column(String(length=45), nullable=False)
    type_hotel = Column(String(length=45), nullable=False)
    description = Column(Text, nullable=False)
    cost = Column(Integer, nullable=False)

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.name
