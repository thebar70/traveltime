import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool
from sqlalchemy.ext.declarative import declarative_base

BASE_DIR = os.getcwd()
Base = declarative_base()
DATABASE = os.path.join(BASE_DIR, 'db_hotel.sqlite')

from .models import TableHotel


class DataBase():
    """Clase encargada de la manipulacion de la base de datos"""

    def __init__(self):
        """Inicializa la clase DataBase
        define el motor y habilita el uso de hilos para la conexión
        """
        engine = create_engine(
            'sqlite:////' + DATABASE,
            echo=False,
            connect_args={'check_same_thread':False},
            poolclass=StaticPool
        )
        Base.metadata.create_all(engine)
        session = sessionmaker(bind=engine)
        self.session = session()

    def create_hotel(self, hotel):
        """Crear un registro hotel
        parametros:
            instancia hotel
        """
        record = TableHotel(**vars(hotel))
        self.session.add(record)
        self.session.commit()
        self.session.close()

    def update_hotel(self, id, hotel):
        """Actualiza un registro hotel
        parametros:
            id del hotel
            instancia hotel
        """
        self.session.query(TableHotel).filter_by(id=id).update(vars(hotel))
        self.session.commit()
        self.session.close()

    def delete_hotel(self, id):
        """Elimina un registro hotel
        parametros:
            id del hotel
        """
        self.session.query(TableHotel).filter_by(id=id).delete()
        self.session.commit()
        self.session.close()

    def get_hotel_by_id(self, id):
        """Recupera un registro hotel
        parametros:
            id del hotel
        retorna:
            registro del hotel
        """
        record = self.session.query(TableHotel).filter_by(id=id).first()
        self.session.close()
        return record

    def get_hotel_by_name(self, name):
        """Recupera un registro hotel
        parametros:
            nombre del hotel
        retorna:
            registro del hotel
        """
        record = self.session.query(TableHotel).filter_by(name=name).first()
        self.session.close()
        return record

    def get_hotels(self):
        """Recupera todos los registros de hoteles
        retorna:
            lista de registros de hoteles
        """
        records = self.session.query(TableHotel).all()
        self.session.close()
        return records
