from models.models import Hotel
from .utils import transform_to_dict
from .utils import transform_to_list
from .utils import transform_to_hotel
from persistence.database import DataBase

# Create yours controllers here


class Controller:
    """Clase Controlador
    intermediario entre el persistencia, modelos y servidor
    """

    def __init__(self):
        """Inicializa la clase Controlador"""
        self.db = DataBase()

    def create_hotel(self, dict_hotel):
        """Crea un hotel
        recibe un diccionario con los datos de hotel
        """
        hotel = transform_to_hotel(dict_hotel)
        self.db.create_hotel(hotel)
        return True

    def update_hotel(self, dict_hotel):
        """Actualiza un hotel
        recibe un diccionario con los datos de hotel
        """
        id = dict_hotel.pop('id')
        hotel = transform_to_hotel(dict_hotel)
        self.db.update_hotel(id, hotel)
        return True

    def delete_hotel(self, id):
        """Elimina un hotel, recibe el id del hotel
        """
        self.db.delete_hotel(id)

    def get_hotel_by_id(self, id):
        """Retorna un hotel, recibe el id del hotel"""
        hotel = self.db.get_hotel_by_id(id)
        return transform_to_dict(hotel)

    def get_hotel_by_name(self, name):
        """Retorna un hotel, recibe un nombre"""
        hotel = self.db.get_hotel_by_name(name.lower())
        return transform_to_dict(hotel)

    def get_hotels(self):
        """Retorna todos los hoteles"""
        hotels = self.db.get_hotels()
        return transform_to_list(hotels)
