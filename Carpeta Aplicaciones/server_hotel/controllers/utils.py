from models.models import Hotel

# Methods utilities


def transform_to_hotel(dict_hotel):
    """Transforma un diccionario a hotel
    parametros:
        diccionario con los datos de hotel
    retorna:
        instancia de hotel
    """
    for key, value in dict_hotel.items():
        if isinstance(value, str):
            dict_hotel.update({key: value.lower()})
    return Hotel(**dict_hotel)

def transform_to_dict(hotel):
    """Transforma un hotel a diccionario
    parametros:
        instancia de hotel
    retorna:
        diccionario con los datos de hotel
    """
    if not hotel:
        return None
    dict = {
        'id': hotel.id,
        'name': hotel.name,
        'type_hotel': hotel.type_hotel,
        'description': hotel.description,
        'cost': hotel.cost
    }
    return dict

def transform_to_list(hotels):
    """Agrupa los hoteles a una lista
    parametros:
        lista de registros de hoteles
    retorna:
        lista de diccionarios con los datos de hoteles
    """
    list_hotels = []
    for hotel in hotels:
        list_hotels.append(transform_to_dict(hotel))
    return list_hotels
