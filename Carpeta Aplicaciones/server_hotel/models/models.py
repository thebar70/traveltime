

# Create yours models here

class Hotel:
    """Clase Hotel"""

    def __init__(self, **kwargs):
        """Inicializa la clase Hotel
        atributos:
            name
            type_hotel
            description
            cost
        """
        self.name = kwargs.get('name')
        self.type_hotel = kwargs.get('type_hotel')
        self.description = kwargs.get('description')
        self.cost = kwargs.get('cost')
