from models.models import Hotel
from controllers.controllers import Controller


if __name__ == '__main__':
    """Funcion principal del servidor de hoteles"""

    controller = Controller()

    hotel = Hotel(
        name='Sol Caribe San Andres',
        type_hotel='Sencilla',
        description='Cama, TV, Aire Acondicionado',
        cost=90000
    )
    hotel1 = Hotel(
        name='Tequendama',
        type_hotel='Precidencial',
        description='Cama, TV, Aire Acondicionado',
        cost=190000
    )
    hotel2 = Hotel(
        name='Torre hotel',
        type_hotel='Sencilla',
        description='Cama, TV, Aire Acondicionado',
        cost=90000
    )

    # Transforma un hotel a diccionario
    controller.create_hotel(vars(hotel))
    controller.create_hotel(vars(hotel1))
    controller.create_hotel(vars(hotel2))

    # Retorna un hotel por medio del nombre
    hotel = controller.get_hotel_by_name('Sol Caribe San Andres')
    print(hotel)
