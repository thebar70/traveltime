import Pyro4

from controllers.controllers import Controller

# Config server hotel


@Pyro4.expose
class ServerHotel:
    """Clase Servidor Hotel"""

    def __init__(self):
        """Inicializa la clase Servidor Hotel
        asigna:
            instancia de Controlador
        """
        self.controller = Controller()

    def create_hotel(self, dic):
        """Crea un hotel
        parametros:
            diccionario con los datos de hotel
        retorna:
            instancia hotel
        """
        return self.controller.create_hotel(dic)

    def update_hotel(self, dic):
        """Actualiza un hotel
        parametros:
            diccionario con los datos de hotel
        retorna:
            diccionario con los datos de hotel
        """
        return self.controller.update_hotel(dic)

    def delete_hotel(self, id):
        """Elimina un hotel
        parametros:
            id del hotel
        retorna:
            diccionario con los datos de hotel
        """
        return self.controller.delete_hotel(id)

    def get_hotel_by_id(self, id):
        """Retorna un hotel
        parametros:
            id de hotel
        retorna:
            diccionario con los datos de hotel
        """
        return self.controller.get_hotel_by_id(id)

    def get_hotel_by_name(self, name):
        """Retorna un hotel
        parametros:
            nombre del hotel
        retorna:
            diccionario con los datos de hotel
        """
        return self.controller.get_hotel_by_name(name)

    def get_hotels(self):
        """Retorna todos los hoteles
        retorna:
            lista de diccionarios con los datos de hoteles
        """
        return self.controller.get_hotels()


def run():
    """Metodo que registra la clase Servidor Hotel
    en el demonio de Pyro4
    """
    objects = {
        ServerHotel: 'server_hotel'
    }
    Pyro4.Daemon.serveSimple(objects)


if __name__ == '__main__':
    run()
