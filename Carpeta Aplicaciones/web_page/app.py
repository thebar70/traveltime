from flask import Flask
from flask import render_template
from flask_sqlalchemy import SQLAlchemy

from config import ConfigDevelopment

app = Flask(__name__)
db = SQLAlchemy()

class Package(db.Model):
    """
    Modelo Package
    """

    id_package = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(45), nullable=False)
    city = db.Column(db.String(45), nullable=False)
    region = db.Column(db.String(45), nullable=False)

    def __repr__(self):
        return self.name



@app.route('/')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.config.from_object(ConfigDevelopment())

    db.init_app(app)
    with app.app_context():
        db.create_all()
    app.run()
