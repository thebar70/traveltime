import os

BASE_DIR = os.getcwd()


class ConfigDevelopment():
    """
    Configuración para desarrollo
        DEBUG = True
        WTF_CSRF_ENABLED = False
        DATABASE = db
    """
    DEBUG = True
    WTF_CSRF_ENABLED = False

    DATABASE = os.path.join(BASE_DIR, 'db.sqlite3')
    SQLALCHEMY_DATABASE_URI = 'sqlite:////' + DATABASE
    SQLALCHEMY_TRACK_MODIFICATIONS = False
