# Web Page

Travel application for Ingenieria de Software II

## Steps required to start application

1. It is recommended to use **Python 3.4** or higher
2. Install the packages contained in **requirements.txt** using the command installation of `pip install -r requirements.txt`
3. Execute the main.py file with `python main.py`
4. Open the browser and go to the address http://localhost:5000/ or http://127.0.0.1:5000/
