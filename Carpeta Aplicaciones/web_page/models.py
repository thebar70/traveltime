from app import db


class Package(db.Model):
    """
    Modelo Package
    """

    id_package = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(45), nullable=False)
    city = db.Column(db.String(45), nullable=False)
    region = db.Column(db.String(45), nullable=False)

    def __repr__(self):
        return self.name
