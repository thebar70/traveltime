

# Create yours models here

class Client:
    """Clase Cliente"""

    def __init__(self, **kwargs):
        """Inicializa la clase Cliente
        atributos:
            id
            names
            last_names
            birthdate
            email
            gender
            address
            city_residence
            telephone
        """
        self.id = kwargs.get('id')
        self.names = kwargs.get('names')
        self.last_names = kwargs.get('last_names')
        self.birthdate = kwargs.get('birthdate')
        self.email = kwargs.get('email')
        self.gender = kwargs.get('gender')
        self.address = kwargs.get('address')
        self.city_residence = kwargs.get('city_residence')
        self.telephone = kwargs.get('telephone')


class Region:
    """Clase Region"""

    def __init__(self, **kwargs):
        """Inicializa la clase Region
        atributos:
            id
            name
            cost
        """
        self.id = kwargs.get('id')
        self.name = kwargs.get('name')
        self.cost = kwargs.get('cost')


class Country:
    """Clase Pais"""

    def __init__(self, **kwargs):
        """Inicializa la clase Region
        atributos:
            id
            region_id
            name
            cost
        """
        self.id = kwargs.get('id')
        self.region_id = kwargs.get('region_id')
        self.name = kwargs.get('name')
        self.cost = kwargs.get('cost')


class City:
    """Clase Ciudad"""

    def __init__(self, **kwargs):
        """Inicializa la clase Region
        atributos:
            id
            country_id
            name
            cost
        """
        self.id = kwargs.get('id')
        self.country_id = kwargs.get('country_id')
        self.name = kwargs.get('name')
        self.cost = kwargs.get('cost')


class Activity:
    """Clase Actividad"""

    def __init__(self, **kwargs):
        """Inicializa la clase Region
        atributos:
            id
            city_id
            name
            description
            cost
        """
        self.id = kwargs.get('id')
        self.city_id = kwargs.get('city_id')
        self.name = kwargs.get('name')
        self.description = kwargs.get('description')
        self.cost = kwargs.get('cost')


class Package:
    """Clase Paquete"""

    def __init__(self, **kwargs):
        """Inicializa la clase Region
        atributos:
            id
            date
            cost
            number_people
            client_id
            regions
            countrys
            citys
            activitys
            hotels
            airlines
        """
        self.id = kwargs.get('id')
        self.date = kwargs.get('date')
        self.cost= kwargs.get('cost')
        self.number_people = kwargs.get('number_people')
        self.client_id = kwargs.get('client_id')
        self.regions = kwargs.get('regions')
        self.countrys = kwargs.get('countrys')
        self.citys = kwargs.get('citys')
        self.activitys = kwargs.get('activitys')
        self.hotels = kwargs.get('hotels')
        self.airlines = kwargs.get('airlines')
