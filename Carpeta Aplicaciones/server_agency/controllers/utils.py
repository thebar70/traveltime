from models.models import Client
from models.models import Region
from models.models import Country
from models.models import City
from models.models import Activity
from models.models import Package

# Methods utilities


def transform_to_client(dict_client):
    """Transforma un diccionario a cliente
    parametros:
        diccionario con los datos de cliente
    retorna:
        instancia de cliente
    """
    for key, value in dict_client.items():
        if isinstance(value, str):
            dict_client.update({key: value.lower()})
    return Client(**dict_client)

def transform_to_dict(client):
    """Transforma un cliente a diccionario
    parametros:
        instancia de cliente
    retorna:
        diccionario con los datos de cliente
    """
    if not client:
        return None
    dic = {
        'id': client.id,
        'names': client.names,
        'last_names': client.last_names,
        'birthdate': client.birthdate,
        'email': client.email,
        'gender': client.gender,
        'address': client.address,
        'city_residence': client.city_residence,
        'telephone': client.telephone,
    }
    return dic

def transform_to_list(clients):
    """Agrupa los clientes a una lista
    parametros:
        lista de registros de clientes
    retorna:
        lista de diccionarios con los datos de todos los clientes
    """
    list_clients = []
    for client in clients:
        list_clients.append(transform_to_dict(client))
    return list_clients

def transform_to_dict_reg(reg):
    """Transforma una region a diccionario
    parametros:
        instancia de region
    retorna:
        diccionario con los datos de region
    """
    if not reg:
        return None
    dic = {
        'id': reg.id,
        'name': reg.name,
        'cost': reg.cost,
    }
    return dic

def transform_to_region(dict_region):
    """Transforma una region a diccionario
    parametros:
        instancia de region
    retorna:
        diccionario con los datos de region
    """
    for key, value in dict_region.items():
        if isinstance(value, str):
            dict_region.update({key: value.lower()})
    return Region(**dict_region)

def transform_to_list_regions(regions):
    """Agrupa las regiones a una lista
    parametros:
        lista de registros de regiones
    retorna:
        lista de diccionarios con los datos de todas las regiones
    """
    list_regions = []
    for reg in regions:
        list_regions.append(transform_to_dict_reg(reg))
    return list_regions

def transform_to_dict_country(country):
    """Transforma un pais a diccionario
    parametros:
        instancia de pais
    retorna:
        diccionario con los datos de pais
    """
    if not country:
        return None
    dic = {
        'id': country.id,
        'region_id': country.region_id,
        'name': country.name,
        'cost': country.cost,
    }
    return dic

def transform_to_country(dict_country):
    """Transforma un diccionario a pais
    parametros:
        diccionario con los datos de pais
    retorna:
        instancia de pais
    """
    for key, value in dict_country.items():
        if isinstance(value, str):
            dict_country.update({key: value.lower()})
    return Country(**dict_country)

def transform_to_list_countrys(countrys):
    """Agrupa los paises a una lista
    parametros:
        lista de registros de paises
    retorna:
        lista de diccionarios con los datos de todos los paises
    """
    list_countrys = []
    for reg in countrys:
        list_countrys.append(transform_to_dict_country(reg))
    return list_countrys

def transform_to_dict_city(city):
    """Transforma una ciudad a diccionario
    parametros:
        instancia de ciudad
    retorna:
        diccionario con los datos de ciudad
    """
    if not city:
        return None
    dic = {
        'id': city.id,
        'country_id': city.country_id,
        'name': city.name,
        'cost': city.cost,
    }
    return dic

def transform_to_city(dict_city):
    """Transforma un diccionario a ciudad
    parametros:
        diccionario con los datos de ciudad
    retorna:
        instancia de ciudad
    """
    for key, value in dict_city.items():
        if isinstance(value, str):
            dict_city.update({key: value.lower()})
    return City(**dict_city)

def transform_to_list_citys(citys):
    """Agrupa las ciudades a una lista
    parametros:
        lista de registros de ciudades
    retorna:
        lista de diccionarios con los datos de todas las ciudades
    """
    list_citys = []
    for reg in citys:
        list_citys.append(transform_to_dict_city(reg))
    return list_citys

def transform_to_dict_activity(activity):
    """Transforma un diccionario a actividad
    parametros:
        instancia de actividad
    retorna:
        diccionario con los datos de actividad
    """
    if not activity:
        return None
    dic = {
        'id': activity.id,
        'city_id': activity.city_id,
        'name': activity.name,
        'description':activity.description,
        'cost': activity.cost,
    }
    return dic

def transform_to_activity(dict_activity):
    """Transforma un diccionario a actividad
    parametros:
        diccionario con los datos de actividad
    retorna:
        instancia de actividad
    """
    for key, value in dict_activity.items():
        if isinstance(value, str):
            dict_activity.update({key: value.lower()})
    return Activity(**dict_activity)

def transform_to_list_activitys(activitys):
    """Agrupa las actividades a una lista
    parametros:
        lista de registros de actividades
    retorna:
        lista de diccionarios con los datos de todos las actividades
    """
    list_activitys = []
    for reg in activitys:
        list_activitys.append(transform_to_dict_activity(reg))
    return list_activitys

def transform_to_dict_package(package):
    """Transforma un paquete a diccionario
    parametros:
        instancia de paquete
    retorna:
        diccionario con los datos de paquete
    """
    if not package:
        return None
    dic = {
        'id': package.id,
        'date':package.date,
        'cost':package.cost,
        'number_people':package.number_people,
        'client_id': package.client_id,
        'regions':package.regions,
        'countrys':package.countrys,
        'citys':package.citys,
        'activitys':package.activitys,
        'hotels':package.hotels,
        'airlines':package.airlines,
    }
    return dic

def transform_to_package(dict_package):
    """Transforma un diccionario a paquete
    parametros:
        diccionario con los datos de paquete
    retorna:
        instancia de paquete
    """
    for key, value in dict_package.items():
        if isinstance(value, str):
            dict_package.update({key: value.lower()})
    return Package(**dict_package)

def transform_to_list_packages(packages):
    """Agrupa los paquetes a una lista
    parametros:
        lista de registros de paquetes
    retorna:
        lista de diccionarios con los datos de todos los paquetes
    """
    list_packages = []
    for reg in packages:
        list_packages.append(transform_to_dict_package(reg))
    return list_packages
