from models.models import Client
from .utils import transform_to_dict
from .utils import transform_to_list
from .utils import transform_to_client

from .utils import *

from persistence.database import DataBase

# Create yours controllers here


class Controller:
    """Clase Controlador"""

    def __init__(self):
        """Inicializa la clase Controlador
        asigna:
            instancia de la base de datos
        """
        self.db = DataBase()

    def create_client(self, dict_client):
        """Crea un cliente
        parametros:
            diccionario con los datos de cliente
        retorna:
            Booleano
        """
        client = transform_to_client(dict_client)
        self.db.create_client(client)
        return True

    def update_client(self, dict_client):
        """Actualiza un cliente
        parametros:
            diccionario con los datos de cliente
        retorna:
            Booleano
        """
        client = transform_to_client(dict_client)
        self.db.update_client(client)
        return True

    def delete_client(self, id):
        """Elimina un cliente
        parametros:
            id del cliente
        retorna:
            Booleano
        """
        self.db.delete_client(id)

    def get_client_by_id(self, id):
        """Recupera un cliente
        parametros:
            id del cliente
        retorna:
            diccionario con los datos de cliente
        """
        client = self.db.get_client_by_id(id)
        return transform_to_dict(client)

    def get_client_by_name(self, name):
        """Recupera un cliente
        parametros:
            nombre del cliente
        retorna:
            diccionario con los datos de cliente
        """
        client = self.db.get_client_by_name(name.lower())
        return transform_to_dict(client)

    def get_clients(self):
        """Recupera todos los clientes
        retorna:
            lista de diccionarios con los datos de clientes
        """
        clients = self.db.get_clients()
        return transform_to_list(clients)

    def get_regions(self):
        """Recupera todas las regiones
        retorna:
            lista de diccionarios con los datos de clientes
        """
        regions = self.db.get_regions()
        return transform_to_list_regions(regions)

    def create_region(self, dict_region):
        """Crea una region
        retorna:
            Booleano
        """
        region = transform_to_region(dict_region)
        self.db.create_region(region)
        return True

    def get_countrys(self, reg_id):
        """Recupera todas los paises
        parametros:
            id de la region
        retorna:
            lista de diccionarios con los datos de paises
        """
        countrys = self.db.get_countrys(reg_id)
        return transform_to_list_countrys(countrys)

    def create_country(self, dict_country):
        """Crea un pais
        parametros:
            diccionario con los datos de pais
        retorna:
            Booleano
        """
        country = transform_to_country(dict_country)
        self.db.create_country(country)
        return True

    def get_citys(self, country_id):
        """Recupera todas las ciudades
        parametros:
            id de la region
        retorna:
            diccionario con los datos de pais
        """
        citys = self.db.get_citys(country_id)
        return transform_to_list_citys(citys)

    def create_city(self, dict_city):
        """Crea una ciudad
        parametros:
            diccionario con los datos de ciudad
        retorna:
            Booleano
        """
        city = transform_to_city(dict_city)
        self.db.create_city(city)
        return True

    def get_activitys(self, city_id):
        """Recupera una actividad
        parametros:
            id de la ciudad
        retorna:
            diccionario con los datos de ciudad
        """
        activitys = self.db.get_activitys(city_id)
        return transform_to_list_activitys(activitys)

    def create_activity(self, dict_activity):
        """Crea una actividad
        parametros:
            diccionario con los datos de actividad
        retorna:
            Booleano
        """
        activity = transform_to_activity(dict_activity)
        self.db.create_activity(activity)
        return True

    def get_packages(self,client_id):
        """Recupera todos los paquetes
        parametros:
            id del cliente
        retorna:
            lista de diccionarios con los datos de paquetes
        """
        packages = self.db.get_packages(client_id)
        return transform_to_list_packages(packages)

    def get_package_by_id(self, id):
        """Recupera un paquete
        parametros:
            id del paquete
        retorna:
            diccionario con los datos de paquete
        """
        package = self.db.get_package_by_id(id)
        return transform_to_dict_package(package)

    def create_package(self, dict_package):
        """Crea una paquete
        parametros:
            diccionario con los datos de paquete
        retorna:
            Booleano
        """
        package = transform_to_package(dict_package)
        self.db.create_package(package)
        return True
