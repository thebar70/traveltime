import Pyro4
from controllers.controllers import Controller

# Config server agency


@Pyro4.expose
class ServerAgency:
    """Clase Servidor Agencia"""

    def __init__(self):
        """Inicializa la clase Servidor Hotel
        asigna:
            instancia de Controlador
        """
        self.controller = Controller()

    def create_client(self, dic):
        """Crea un cliente
        parametros:
            diccionario con los datos de cliente
        retorna:
            Booleano
        """
        return self.controller.create_client(dic)

    def update_client(self, dic):
        """Actualiza un cliente
        parametros:
            diccionario con los datos de cliente
        retorna:
            Booleano
        """
        return self.controller.update_client(dic)

    def delete_client(self, id):
        """Elimina un cliente
        parametros:
            id del cliente
        retorna:
            Booleano
        """
        return self.controller.delete_client(id)

    def get_client_by_id(self, id):
        """Recupera un cliente
        parametros:
            id del cliente
        retorna:
            diccionario con los datos de cliente
        """
        return self.controller.get_client_by_id(id)

    def get_client_by_name(self, name):
        """Recupera un cliente
        parametros:
            nombre del cliente
        retorna:
            diccionario con los datos de cliente
        """
        return self.controller.get_client_by_name(name)

    def get_clients(self):
        """Recupera todos los clientes
        retorna:
            lista de diccionarios con los datos de todos los clientes
        """
        return self.controller.get_clients()

    def get_hotels(self):
        """Recupera todos los hoteles desde el Servidor Hotel
        retorna:
            lista de diccionarios con los datos de todos los clientes
        """
        name_server = Pyro4.locateNS()
        server_hotel_uri = name_server.lookup('server_hotel')
        server_hotel_remote = Pyro4.Proxy(server_hotel_uri)
        return server_hotel_remote.get_hotels()

    def get_regions(self):
        """Recupera todas las regiones
        retorna:
            lista de diccionarios con los datos de todas las regiones
        """
        return self.controller.get_regions()

    def create_region(self, dic):
        """Crea una region
        parametros:
            diccionario con los datos de region
        retorna:
            Booleano
        """
        return self.controller.create_region(dic)

    def get_countrys(self, reg_id):
        """Recupera todos los paises
        parametros:
            id de la region
        retorna:
            lista de diccionarios con los datos de todos los paises
        """
        return self.controller.get_countrys(reg_id)

    def create_country(self, dic):
        """Crea un pais
        parametros:
            diccionario con los datos de pais
        retorna:
            Booleano
        """
        return self.controller.create_country(dic)

    def get_citys(self, country_id):
        """Recupera todas las ciudades
        parametros:
            id del pais
        retorna:
            lista de diccionarios con los datos de ciudades
        """
        return self.controller.get_citys(country_id)

    def create_city(self, dic):
        """Crea una ciudad
        parametros:
            diccionario con los datos de ciudad
        retorna:
            Booleano
        """
        return self.controller.create_city(dic)

    def get_activitys(self, city_id):
        """Recupera todas las actividades
        parametros:
            id de la ciudad
        retorna:
            lista de diccionarios con los datos de actividades
        """
        return self.controller.get_activitys(city_id)

    def create_activity(self, dic):
        """Crea una actividad
        parametros:
            diccionario con los datos de actividad
        retorna:
            Booleano
        """
        return self.controller.create_activity(dic)

    def create_package(self, dic):
        """Crea una paquete
        parametros:
            diccionario con los datos de paquete
        retorna:
            Booleano
        """
        return self.controller.create_package(dic)

    def get_packages(self, client_id):
        """Recupera todos los paquetes
        parametros:
            id del cliente
        retorna:
            lista de diccionarios con los datos de todos los paquetes
        """
        return self.controller.get_packages(client_id)

    def get_package_by_id(self, id):
        """Recupera un paquete
        parametros:
            id del paquete
        retorna:
            diccionario con los datos de paquete
        """
        return self.controller.get_package_by_id(id)


def run():
    """Metodo que registra la clase Servidor Agencia
    en el demonio de Pyro4
    """
    objects = {
        ServerAgency: 'server_agency'
    }
    Pyro4.Daemon.serveSimple(objects)


if __name__ == '__main__':
    run()
