import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool
from sqlalchemy.ext.declarative import declarative_base

BASE_DIR = os.getcwd()
Base = declarative_base()
DATABASE = os.path.join(BASE_DIR, 'db_agency.sqlite')

from .models import TableClient
from .models import TableRegion
from .models import TableCountry
from .models import TableCity
from .models import TableActivity
from .models import TablePackage


class DataBase():
    """Clase encargada de la manipulacion de la base de datos"""

    def __init__(self):
        """Inicializa la clase DataBase
        define el motor y habilita el uso de hilos para la conexión
        """
        engine = create_engine(
            'sqlite:////' + DATABASE,
            echo=False,
            connect_args={'check_same_thread':False},
            poolclass=StaticPool
        )
        Base.metadata.create_all(engine)
        session = sessionmaker(bind=engine)
        self.session = session()

    def create_client(self, client):
        """Crear un registro cliente
        parametros:
            instancia hotel
        """
        record = TableClient(**vars(client))
        self.session.add(record)
        self.session.commit()
        self.session.close()

    def update_client(self, client):
        """Actualiza un registro cliente
        parametros:
            instancia cliente
        """
        self.session.query(TableClient).filter_by(id=client.id).update(vars(client))
        self.session.commit()
        self.session.close()

    def delete_client(self, id):
        """Elimina un registro cliente
        parametros:
            id del cliente
        """
        self.session.query(TableClient).filter_by(id=id).delete()
        self.session.commit()
        self.session.close()

    def get_client_by_id(self, id):
        """Recupera un registro cliente
        parametros:
            id del cliente
        retorna:
            registro cliente
        """
        record = self.session.query(TableClient).filter_by(id=id).first()
        self.session.close()
        return record

    def get_client_by_name(self, name):
        """Recupera un registro cliente
        parametros:
            nombre del cliente
        retorna:
            registro cliente
        """
        record = self.session.query(TableClient).filter_by(names=name).first()
        self.session.close()
        return record

    def get_clients(self):
        """Recupera todos los registro de clientes
        retorna:
            lista de registros de clientes
        """
        records = self.session.query(TableClient).all()
        self.session.close()
        return records

    def get_regions(self):
        """Recupera todos los registro de regiones
        retorna:
            lista de registros de regiones
        """
        records = self.session.query(TableRegion).all()
        self.session.close()
        return records

    def create_region(self, region):
        """Crea un registro region
        parametros:
            instancia region
        """
        record = TableRegion(**vars(region))
        self.session.add(record)
        self.session.commit()
        self.session.close()

    def get_countrys(self, reg_id):
        """Recupera todos los registro de paises
        retorna:
            lista de registros de paises
        """
        countrys=self.session.query(TableCountry).filter_by(region_id=reg_id).all()
        self.session.close()
        return countrys

    def create_country(self, country):
        """Crea un registro pais
        parametros:
            instancia pais
        """
        record = TableCountry(**vars(country))
        self.session.add(record)
        self.session.commit()
        self.session.close()

    def get_citys(self,country_id):
        """Recupera todos los registro de ciudades
        retorna:
            lista de registros de ciudades
        """
        records = self.session.query(TableCity).filter_by(country_id=country_id).all()
        self.session.close()
        return records

    def create_city(self, city):
        """Crea un registro ciudad
        parametros:
            instancia ciudad
        """
        record = TableCity(**vars(city))
        self.session.add(record)
        self.session.commit()
        self.session.close()

    def get_activitys(self,city_id):
        """Recupera todos los registro de actividades
        retorna:
            lista de registros de actividades
        """
        records = self.session.query(TableActivity).filter_by(city_id=city_id).all()
        self.session.close()
        return records

    def create_activity(self, activity):
        """Crea un registro actividad
        parametros:
            instancia actividad
        """
        record = TableActivity(**vars(activity))
        self.session.add(record)
        self.session.commit()
        self.session.close()

    def create_package(self,package):
        """Crea un registro paquete
        parametros:
            instancia paquete
        """
        record = TablePackage(**vars(package))
        self.session.add(record)
        self.session.commit()
        self.session.close()

    def get_packages(self,client_id):
        """Recupera todos los registro de paquetes
        retorna:
            lista de registros de paquetes
        """
        records = self.session.query(TablePackage).filter_by(client_id=client_id).all()
        self.session.close()
        return records

    def get_package_by_id(self, id):
        """Recupera un registro paquete
        parametros:
            id del paquete
        retorna:
            registro paquete
        """
        record = self.session.query(TablePackage).filter_by(id=id).first()
        self.session.close()
        return record
