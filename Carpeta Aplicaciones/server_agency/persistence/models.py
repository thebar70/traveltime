from sqlalchemy import Table
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import CHAR
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from .database import Base

# Create yours models here


class TableLog(Base):
    """Clase Registro que es mapeada a la base de datos.
    campos:
        id
        name
        names
        password
        timestamp
    """

    __tablename__ = 'log'

    id = Column(Integer, primary_key=True)
    name = Column(String(length=45), nullable=False)
    names = Column(String(length=45), nullable=False)
    password = Column(String(length=45), nullable=False)
    timestamp = Column(DateTime, nullable=False)

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.name


class TableClient(Base):
    """Clase Cliente que es mapeada a la base de datos.
    campos:
        id
        names
        last_names
        birthdate
        email
        gender
        address
        city_residence
        telephone
    """

    __tablename__ = 'client'

    id = Column(Integer, primary_key=True)
    names = Column(String(length=45), nullable=False)
    last_names = Column(String(length=45), nullable=False)
    birthdate = Column(String(length=45))
    email = Column(String(length=45), nullable=False)
    gender = Column(CHAR, nullable=False)
    address = Column(String(length=45))
    city_residence = Column(String(length=45))
    telephone = Column(Integer)

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.names


class TableRegion(Base):
    """Clase Cliente que es mapeada a la base de datos.
    campos:
        id
        name
        cost
    """

    __tablename__ = 'region'

    id = Column(Integer, primary_key=True)
    name = Column(String(length=45), nullable=False)
    cost = Column(Integer, nullable=False)

    countrys = relationship("TableCountry")

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.name


class TableCountry(Base):
    """Clase Pais que es mapeada a la base de datos.
    campos:
        id
        name
        cost
        region_id
    """

    __tablename__ = 'country'

    id = Column(Integer, primary_key=True)
    name = Column(String(length=45), nullable=False)
    cost = Column(Integer, nullable=False)

    region_id = Column(Integer, ForeignKey('region.id'))
    citys = relationship("TableCity")

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.name


class TableCity(Base):
    """Clase Ciudad que es mapeada a la base de datos.
    campos:
        id
        name
        cost
        country_id
        activitys
    """

    __tablename__ = 'city'

    id = Column(Integer, primary_key=True)
    name = Column(String(length=45), nullable=False)
    cost = Column(Integer, nullable=False)

    country_id = Column(Integer, ForeignKey('country.id'))
    activitys = relationship("TableActivity")

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.name


class TableActivity(Base):
    """Clase Actividad que es mapeada a la base de datos.
    campos:
        id
        name
        description
        cost
        city_id
    """

    __tablename__ = 'activity'

    id = Column(Integer, primary_key=True)
    name = Column(String(length=45), nullable=False)
    description = Column(String(length=250), nullable=False)
    cost = Column(Integer, nullable=False)

    city_id = Column(Integer, ForeignKey('city.id'))

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.name

class TablePackage(Base):
    """Clase Paquete que es mapeada a la base de datos.
    campos:
        id
        date
        cost
        number_people
        client_id
        regions
        countrys
        citys
        activitys
        hotels
        airlines
    """

    __tablename__ = 'package'

    id = Column(Integer, primary_key=True)
    date = Column(String(length=50), nullable=False)
    cost = Column(Integer, nullable=False)
    number_people = Column(Integer, nullable=False)
    client_id = Column(Integer,nullable=False)
    regions = Column(String(length=50), nullable=False)
    countrys = Column(String(length=50), nullable=False)
    citys = Column(String(length=50), nullable=False)
    activitys = Column(String(length=50), nullable=False)
    hotels = Column(String(length=50), nullable=False)
    airlines = Column(String(length=250), nullable=False)

    def __repr__(self):
        """Retorna una cadena que describe al objeto"""
        return self.name
